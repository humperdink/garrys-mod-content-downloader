<#
.Notes
=====================================================
   Created on: 09/27/2022
   Created by: humperdink
   Filename:   GarrysModContentDownloader.ps1
   Version:    1.5
   Requires:   - PowerShell version 5.0 and up
               - Carbon Module
=====================================================
.Synopsis
   Content downloader for Garrys Mod
.DESCRIPTION
   Downloads extra content from other games for use in Garrys Mod -- Created for prop hunt purposes
.EXAMPLE
   Run script and make sure the drive that Garrys Mod is installed on has ~50GB free (unsure of how much is actually needed)
   Script will automatically create directories in Garrys Mod folder, download SteamCMD, download game content, and create a new mount file
#>

Write-Host "Hold on tight! It's going to be a long ride" -BackgroundColor Red -ForegroundColor White
Start-Sleep -Seconds 5
Write-Host "If you are prompted to accept anything make sure you say YES or if it's available say YES TO ALL" -BackgroundColor Red -ForegroundColor White
Start-Sleep -Seconds 10

# Checks if Carbon module is installed and if it is not then it will install it
Write-Host "Checking if Carbon is installed..." -BackgroundColor Red -ForegroundColor White
if (Get-Module -ListAvailable -Name Carbon) {
    Write-Host "Carbon is installed" -BackgroundColor Red -ForegroundColor White
}
else {
    Write-Host "Carbon is not installed" -BackgroundColor Red -ForegroundColor White
    Write-Host "Installing Carbon..." -BackgroundColor Red -ForegroundColor White
    Install-Module -Name 'Carbon' -AllowClobber
}

# Attempts to import the Carbon module and if it does not then it exits the script
Write-Host "Importing Carbon..." -BackgroundColor Red -ForegroundColor White
Import-Module 'Carbon'

Start-Sleep -Seconds 5

if (Get-Module -Name Carbon) {
    Write-Host "Carbon was imported successfully..." -BackgroundColor Red -ForegroundColor White
}
else {
    Write-Host "Carbon was not imported successfully" -BackgroundColor Red -ForegroundColor White
    pause
}

# Get the install path for Garrys Mod as a variable and set to main directory
$GmodInstall = (Get-CProgramInstallInfo -Name "*Garry's*Mod*").InstallLocation
Set-Location -Path "$GmodInstall"

# Create directories for content and download SteamCMD
Write-Host 'Creating lots of folders for your glorious content...' -BackgroundColor Red -ForegroundColor White
New-Item -Name 'GarrysModContent' -ItemType 'directory'
Set-Location -Path 'GarrysModContent'
Write-Host 'Getting SteamCMD...' -BackgroundColor Red -ForegroundColor White
Invoke-WebRequest -Uri 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip' -OutFile 'steamcmd.zip'
Expand-Archive 'steamcmd.zip'
Remove-Item 'steamcmd.zip'
New-Item -Name 'hl2' -ItemType 'directory'
New-Item -Name 'cstrike' -ItemType 'directory'
New-Item -Name 'tf' -ItemType 'directory'
New-Item -Name 'dod' -ItemType 'directory'
New-Item -Name 'hl2mp' -ItemType 'directory'
New-Item -Name 'hl1' -ItemType 'directory'
New-Item -Name 'hl1mp' -ItemType 'directory'
New-Item -Name 'zps' -ItemType 'directory'
New-Item -Name 'left4dead2' -ItemType 'directory'
New-Item -Name 'left4dead' -ItemType 'directory'
New-Item -Name 'csgo' -ItemType 'directory'
Write-Host 'That was the easy part!' -BackgroundColor Red -ForegroundColor White

# Set variables for content download locations and other info
Write-Host 'Just setting up some variables here...' -BackgroundColor Red -ForegroundColor White
$root = Get-Location
$steamcmd = "$root\steamcmd"
$hl2 = "$root\hl2"
$cstrike = "$root\cstrike"
$tf = "$root\tf"
$dod = "$root\dod"
$hl2mp = "$root\hl2mp"
$hl1 = "$root\hl1"
$hl1mp = "$root\hl1mp"
$zps = "$root\zps"
$left4dead2 = "$root\left4dead2"
$left4dead = "$root\left4dead"
$csgo = "$root\csgo"

# Begin downloding content
Write-Host 'This step is going to take a while...' -BackgroundColor Red -ForegroundColor White
Write-Host 'Downloading all of your content...' -BackgroundColor Red -ForegroundColor White
Write-Host 'Go make like 10 cups of coffee...' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Half Life 2' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $hl2 +app_update 232370 validate +quit
Write-Host 'Finished Half Life 2 (1/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Counter Strike' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $cstrike +app_update 232330 validate +quit
Write-Host 'Finished Counter Strike (2/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Team Fortress' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $tf +app_update 232250 validate +quit
Write-Host 'Finished Team Fortress (3/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Day of Defeat' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $dod +app_update 232290 validate +quit
Write-Host 'Finished Day of Defeat (4/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Half Life 2 Multiplayer' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $hl2mp +app_update 232370 validate +quit
Write-Host 'Finished Half Life 2 Multiplayer (5/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Half Life' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $hl1 +app_update 90 validate +quit
Write-Host 'Finished Half Life (6/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Half Life Multiplayer' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $hl1mp +app_update 255470 validate +quit
Write-Host 'Finished Half Life Multiplayer (7/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Zombie Panic Source' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $zps +app_update 17505 validate +quit
Write-Host 'Finished Zombie Panic Source (8/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Left 4 Dead 2' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $left4dead2 +app_update 222860 validate +quit
Write-Host 'Finished Left 4 Dead 2 (9/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Left 4 Dead' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $left4dead +app_update 222840 validate +quit
Write-Host 'Finished Left 4 Dead (10/11)' -BackgroundColor Red -ForegroundColor White
Write-Host 'Starting Counter Strike Global Offensive' -BackgroundColor Red -ForegroundColor White
.\steamcmd\steamcmd.exe +login anonymous +force_install_dir $csgo +app_update 740 validate +quit
Write-Host 'Finished Counter Strike Global offensive (11/11)' -BackgroundColor Red -ForegroundColor White
Write-Host "Yayyyy you're done downloading all of it!" -BackgroundColor Red -ForegroundColor White

# Create content mount file and replace default one
Write-Host 'Making a new mount file for you to use... Your old one sucks!' -BackgroundColor Red -ForegroundColor White
New-Item -Path "$root" -Name 'mount.cfg' -ItemType 'file'
Add-Content .\mount.cfg "`n//`n// Use this file to mount additional paths to the filesystem`n// DO NOT add a slash to the end of the filename`n//`n`n`"mountcfg`"`n{`n`t`"hl2`"`t`"$hl2`"`n`t`"cstrike`"`t`"$cstrike`"`n`t`"tf`"`t`"$tf`"`n`t`"dod`"`t`"$dod`"`n`t`"hl2mp`"`t`"$hl2mp`"`n`t`"hl1`"`t`"$hl1`"`n`t`"hl1mp`"`t`"$hl1mp`"`n`t`"zps`"`t`"$zps`"`n`t`"left4dead2`"`t`"$left4dead2`"`n`t`"left4dead`"`t`"$left4dead`"`n`t`"csgo`"`t`"$csgo`"`n}"
Remove-Item -Path "$GmodInstall\garrysmod\cfg\mount.cfg"
Move-Item -Path "$root\mount.cfg" -Destination "$GmodInstall\garrysmod\cfg\mount.cfg"

Write-Host "That's everything! The mount.cfg file has been moved to your Garry's Mod cfg folder." -BackgroundColor Red -ForegroundColor White

pause
