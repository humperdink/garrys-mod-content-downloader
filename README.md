Make sure the drive that Garry's Mod is installed on has ~50GB of free space (unsure of how much is actually needed)

Put the GarrysModContentDownloader.ps1 file in your Downloads folder

Open PowerShell as an administrator (search for powershell and right click it then click run as administrator)
Run these commands

cd C:\Users\insertyourusernamehere\Downloads

Set-ExecutionPolicy Bypass Process

.\GarrysModContentDownloader.ps1
